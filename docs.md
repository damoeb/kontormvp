# Brainstorming Docs

# Problem Statement

> We don’t produce, we don’t remember, we don’t consume relevant things, we just let us entertain

- Wir verwenden das Internet wie einen Fernseher, eine Zeitung, eine Schreibmaschine
- We consume what others want us to consume

# About
- Personal News Feed based on your identity/ history
- Knowledge Base, Inspiration Base
- Learn 
- collaborative document enrichment

# Goals
1. [Done] Get data from set of homogenic streams (rss?)
2. [Done] Map heterogenic streams (markup) to a standardized format
3. **Identify relevant parts of a large stream consisting of documents**
4. [Done] Read the document in a reader mode
5. [Done] Store documents
6. **Generate custom feeds (if no feed is offered)**
6. **Enrich documents and keep revisions**
7. **Loop back edits and credits**
8. **Collection of documents must be browseable**
9. **Keep everything (like in nvalt) in the doc**

# Features
* Non-proprietary (open source) file formats / standards
	* Based on a filesystem
	* ASCII based content, e.g. Markdown Support (https://github.com/unicate/markdownpages)
* Synchronization and Versioning between devices using Git
* Plugin Support
	* Note livecycle events to hook in plugins
	* https://github.com/neovim/neovim/wiki/Plugin-UI-architecture
* Remote Storage using Git
* Collaboration: “human knowledge is cumulative” implement this idea by encouraging users to make public posts, then other users could search through them and fork them, this will be very good (https://news.ycombinator.com/item?id=9912920)
	* Pull shared git remotes
	* Push connected BEs

* Capture
	* Bookmarklet
	* Global keyboard shortcut like nvALT
* Note
	* Types (Concept)
		* Desk Notes used as entry points
		* Content Notes
		* Structure Notes (zettelkasten-hubs)
	* Structure
		* Title and Content. Title is first line of content
		* Tags: Implicitly like in nvAlt
		* Attachments
		* Links: Folgezettel?
* Inbox for new and scheduled items (e.g. for memorization)
    * shows only a subset of items, the rest can be considered subconscious and 
    will be used for a related item relationship/search
* Content Classification
	* Similar Notes
	* First level notes (actively imported and store notes)
		* Folder structure (like on OS)
		* Tags
			* Colored Labels
	* Second level notes (background notes from channels like RSS, notes from neighbors)
		* Permanent Searches (aka Smart Folders)
* Memorization using Super Memo
* Entry points
	* Full-text Search
	* Index of tags
* User Interface
	* Keyboard shortcuts
	* Web UI
		* Themes
		* The database consists of two parts
		    * Known notes
		    * unknown notes
		  These two worlds should be visible and accessible. The transition from unknown->known happens through the inbox. 
	* CLI (https://github.com/rwilcox/nvalt_cmd_line)
		* Re-used git commands/features:
			* clone
			* init
			* add
			* commit
			* rm
			* reset
			* log
			* pull/push as sync
			* ignore
			* remote
		* Other features
			* search: ft-search
			* alike: search alike notes
			* open: the note using EDITOR
			* link: manually al leen notes
		* ** Open Questions:** Do we need a view mode in the cli to use features like link
	
* Hyperlinks
	* Table of Contents Notes (zettelkasten-hubs)
	* Transclusion to support multiple storage principle

* Privacy
	* Self hosted
	
* User Authentication: Against a BE only and the remote git repo
	* No password auth
* Encryption
	* autocrypt.org
* Import
	* Feeds
	* Channels from Users
* Export

# Inbox Visualiation
The amount of articles will soon be - even with filtering - overwhealming. A visualization that groups articles by keywords might be the only solution.
Crucial to this approach is to classify articles properly into a limited set of a few groups.
Automated keyword extraction is tricky, a simpler approach would be to add static tags for a subscription/feed. This could work for single-type-content feeds (providers). For aggregated feeds this is not necessary, cause they get content from providers, and therefore only contain tagged stuff.


# Lifecycle
- Delete -> Trash (cronjob will clean articles after 60 days)

# Subscribe
It should be called Filter/Smart Folder/Stream or Feed, cause it will also filter to past content and it should be implemented as a normal feed..
To simplify subscriptions, the form fields should be prefilled. A start can be a url, to extract the author and some tags. 
Should be a search field, then depending on input prefills the form in a smart way:
- website-url: 
- feed-url:
- text:

## Form Fields
- What: Best of / everything / contains text Toni Polster
- (Optional) When: Frequency
- From: List of feeds + all stored articles
- Then: Post-Harvest-Steps, add metrics

# Icons
Use http://fontello.com/ to create a custom 

# Spinner
use https://github.com/hakimel/Ladda

# Reader Mode
An article is visualized in 
- a fullscreen mode, that gives you 
  - a good reading experience,
  - with high contrast
  - as little distractions as possible

Good reading experiences
- [wikipedia mobile](https://en.m.wikipedia.org/wiki/Hawthorne_effect)
- [Guardian](https://www.theguardian.com/news/2018/may/10/the-invisible-power-of-big-glasses-eyewear-industry-essilor-luxottica)

# Note Management By Example

- [Luhmanns Zettelkasten](https://zettelkasten.de/) with [Luhmanns principles](https://praxis.fortelabs.co/masters-of-creative-note-taking-luhmann-and-da-vinci-49a887c5e3f/)
- [Leonardo da Vinci's Notebooks](https://www.newyorker.com/magazine/2017/10/16/the-secret-lives-of-leonardo-da-vinci)

````
\#davinci \#knowledge

[Find] a book that treats of Milan and its churches, which is to be had at the stationer’s on the way to Cordusio

[Discover] the measurement of Corte Vecchio (the courtyard in the duke’s palace).

[Talk to] Giannino, the Bombardier, re. the means by which the tower of Ferrara is walled without loopholes 

[Examine] the Crossbow of Mastro Giannetto
````

- [Robert Pirsig's 11.000 slips](https://web.archive.org/web/20160324085123/http://dougtoft.net/2015/04/25/organizing-11000-ideas-how-robert-pirsig-did-it/)

# Typography

## Font
Use *Georgia* font. "while Georgia is classified as a serif font, it was designed for the Internet and for use in online documents so it can be reduced to smaller font sizes, such as 8, and retain its clear, readable structure." (https://www.techwalla.com/articles/the-best-fonts-for-readability)

````
font-face: use woff and woff2, maybe opentype ttf for very old androids
css-font-loader API (standard)
CSS3 font-kerning
text-align: justify
hyphens: auto (define lang on element)
````

Donald Knuths' Computer Modern Font or the [Vollkorn](https://www.fontsquirrel.com/fonts/vollkorn) are good custom alternatives.

http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/

# Monitor
- feed healthiness

# Resources
- [Balancing text for better readability](https://web.archive.org/web/20160304020519/http://blogs.adobe.com/webplatform/2013/01/30/balancing-text-for-better-readability/) with [HN disscussion](https://news.ycombinator.com/item?id=5187936)
- [TeX line breaking algorithm in JavaScript](https://github.com/bramstein/typeset/)
- [5 min on typography](https://www.pierrickcalvez.com/journal/a-five-minutes-guide-to-better-typography) with [HN discussion](https://news.ycombinator.com/item?id=15424478) 
- [Death to Typewriters](https://medium.design/death-to-typewriters-9b7712847639)
- [Typesetting Body Text (Video)](https://vimeo.com/156203722)
- [Web Fonts (Video)](https://www.youtube.com/watch?v=fFqTjWDVb0w)
- [Kerning game](http://type.method.ac/#)

# Related Projects

General
- [Zite.com](https://www.youtube.com/watch?v=LL27VyCN1O4),
- feedsavvy.com
- Flip Board, Feedly
- Feediary.com

Credit
- https://civil.co/white-paper/

Feed Generator
- http://fetchrss.com/

see https://yellowchicken.wordpress.com/2018/07/21/personal-knowledge-base-features/

# Markdown Editors
- [SlackEdit](https://github.com/benweet/stackedit/): great UX, but not embeddable
- [pagedown-ace](https://github.com/benweet/pagedown-ace): Embeddable and based on the popular editor [ACE](https://ace.c9.io/#nav=higlighter)

# Harvest Websites

## HTML to Feed

(see http://fetchrss.com/)
Look for repeating blocks in the markup, that

Rules for a block
- Contains a link
- Link text has at least length n
- Contains a paragraph
- Url points to same page

A block has to occur at least m-times to be relevant

# Security
- All links in reader should be <a :href="url" target="_blank" rel="nofollow noopener noreferrer"></a>

# Quality Metrics
- saves (internal)
- comments (internal + semi-external)
- mentions (external)

# Questions
- a repo is served using the serve command
- which repos are served in the FE?
- How to keep the focus on a topic? Star a tag?
- which user/pass is used?
- Do we need an export, since its exported by default as git repo
- Automated linking: If a note contains a string for which there is a equally named note, they are linked 
- Transclude Notes
- post id 
    - option 1: a unique hash (similar to a commit hash). But, how about linking and transcluding? 
    - option 1: by [username]/title like in nvalt or wikipedia
        - how about renames
        - how about other modifications
        - how about I import that document too, should I then reference my document?
    - a post can have multiple ids
    - hashes are not readable
- post comments and annotations
  Req: content has to be reformatted
 <!-- content:markdown id:body -->

 <!-- content:json id:stats -->
