/**
 * Fade the title on scroll if post has "Post Cover Title Style"
 */
jQuery(function($) {
    var post_title = $('.post-cover-title'),
        post_title_wrapper = $('.post-cover-title-wrapper');

    if($('body').hasClass('post-cover-overlay-post-title')) {
        $(window).on('scroll', function() {
            var st = $(this).scrollTop(),
                post_title_wrapper_height = post_title_wrapper.height(),
                post_title_height = post_title.height(),
                post_title_padding = ( post_title.innerHeight() - post_title_height) / 2;

            post_title.css({
                'margin-bottom' : -(st/post_title_wrapper_height) * post_title_padding +"px",
                'opacity' : 1 - st/post_title_wrapper_height
            });
        });
    }
});
/*
     FILE ARCHIVED ON 16:18:49 Mar 18, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 11:27:06 Jul 12, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 53.242 (3)
  esindex: 0.01
  captures_list: 75.067
  CDXLines.iter: 15.97 (3)
  PetaboxLoader3.datanode: 55.533 (4)
  exclusion.robots: 0.226
  exclusion.robots.policy: 0.209
  RedisCDXSource: 1.695
  PetaboxLoader3.resolve: 103.631
  load_resource: 129.149
*/