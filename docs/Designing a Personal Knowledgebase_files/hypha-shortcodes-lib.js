jQuery(document).ready(function($) {

	$(".hypha-tabs").tabs();
	
	$(".hypha-toggle").each( function () {
		if($(this).attr('data-id') == 'closed') {
			$(this).accordion({ header: '.hypha-toggle-title', collapsible: true, active: false, autoHeight: false });
		} else {
			$(this).accordion({ header: '.hypha-toggle-title', collapsible: true});
		}
	});
	
});
/*
     FILE ARCHIVED ON 16:09:43 Mar 18, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 11:26:30 Jul 12, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 846.078 (3)
  esindex: 0.015
  captures_list: 871.3
  CDXLines.iter: 18.388 (3)
  PetaboxLoader3.datanode: 364.896 (4)
  exclusion.robots: 0.288
  exclusion.robots.policy: 0.267
  RedisCDXSource: 2.143
  PetaboxLoader3.resolve: 675.492 (2)
  load_resource: 240.107
*/