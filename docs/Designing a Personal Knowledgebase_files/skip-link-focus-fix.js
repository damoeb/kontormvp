( function() {
	var is_webkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
	    is_opera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
	    is_ie     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

	if ( ( is_webkit || is_opera || is_ie ) && 'undefined' !== typeof( document.getElementById ) ) {
		var eventMethod = ( window.addEventListener ) ? 'addEventListener' : 'attachEvent';
		window[ eventMethod ]( 'hashchange', function() {
			var element = document.getElementById( location.hash.substring( 1 ) );

			if ( element ) {
				if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) )
					element.tabIndex = -1;

				element.focus();
			}
		}, false );
	}
})();

/*
     FILE ARCHIVED ON 06:29:53 Mar 14, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 11:27:37 Jul 12, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 1151.92 (3)
  esindex: 0.011
  captures_list: 1192.024
  CDXLines.iter: 30.751 (3)
  PetaboxLoader3.datanode: 1183.818 (5)
  exclusion.robots: 0.225
  exclusion.robots.policy: 0.211
  RedisCDXSource: 4.791
  PetaboxLoader3.resolve: 460.53 (2)
  load_resource: 529.665
*/