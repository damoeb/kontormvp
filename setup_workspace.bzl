load("@build_bazel_rules_nodejs//:defs.bzl", "yarn_install")

def worker_setup_workspace():
  """Node repositories for @worker
  """

  yarn_install(
      name = "yarn_worker_deps",
      package_json = "//src/worker:package.json",
      yarn_lock = "//src/worker:yarn.lock",
  )

  yarn_install(
      name = "yarn_server_deps",
      package_json = "//src/server:package.json",
      yarn_lock = "//src/server:yarn.lock",
  )

  yarn_install(
      name = "yarn_client_deps",
      package_json = "//src/client:package.json",
      yarn_lock = "//src/client:yarn.lock",
  )
