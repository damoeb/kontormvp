import {each, times} from 'lodash';
const {argv} = require('yargs').default('taskServer', 'http://localhost:3000');
import * as request from 'request';
import puppeteerService from "./services/puppeteerService";
import {config} from "./config";

// todo default localhost:3000
console.log(`Using task-server ${argv.taskServer}`);
const taskServer = argv.taskServer;

function schedule() {
  setTimeout(it, Math.random() * 3000);
}

function it() {
  request({
    url: `${taskServer}/api/tasks/next`,
    method: 'GET'
  }, (error, response) => {
    if (error) {
      console.log(error.toString());
      setTimeout(it, 10000);

    } else {
      const task = JSON.parse(response.body);

      const startTime = new Date().getTime();
      console.log(`handle ${task.data.articleUrl}`);

      puppeteerService.getReadability(task.data.articleUrl)
        .then(readability => {
          console.log(`replying ${JSON.stringify(readability)} after ${new Date().getTime() - startTime}`);
          return new Promise((resolve) => {
            request({
              url: `${taskServer}/api/tasks/${task._id}`,
              method: 'PUT',
              json: true,
              body: {task, readability}
            }, (error1) => {
              if (error1) {
                console.error(error1);
              }
              resolve();
            })
          });
        })
        .catch(console.error)
        .finally(() => {
          schedule();
        });
    }
  });
}

if (config.debug) {
  console.log('Running in debug mode');
  schedule();
} else {
  times(config.threads).forEach(() => {
    schedule();
  });
}
