<template>
  <div class="container">

    <top-nav></top-nav>

    <div class="columns">
      <div class="column col-2"></div>
      <div class="column col-9">
        <div class="reader" v-bind:class="{ loading: isLoading }">

            <h1>{{title}}</h1>
            <div>by {{by}}</div>
            <div><a :href="url" target="_blank" rel="nofollow noopener noreferrer">Read on Site</a></div>
            <div v-html="content"></div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>

import axios from 'axios'

export default {
  name: 'Reader',
  data () {
    return {
      title: '',
      by: '',
      isLoading: true,
      content: ''
    }
  },
  beforeRouteUpdate (to, from, next) {
    this.url = to.params.url
    next()
  },
  mounted () {
    const articleId = this.$route.params.articleId
    axios(`/api/articles/${articleId}`).then((response) => {
      const data = response.data
      this.title = data.title
      this.content = data.content
      this.url = data.url
      this.by = data.author
      this.isLoading = false
    })
  },
  methods: {
    showError () {
      this.isLoading = false
      console.error('Failed to load article')
    }
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
  .reader {
    font-family: "ComputerModern", Georgia, Palatino, "Century Schoolbook L", "Times New Roman", Times, serif;
    letter-spacing: 0;
    width: 800px;
    font-weight: 400;
    /*text-indent: 1em;*/
    font-style: normal;
    font-kerning: auto;
    text-align: justify;
    text-justify: inter-word;
    hyphens: auto;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -moz-font-feature-settings: "liga" on;

    font-size: medium;
    line-height: 1.4;

    background: white;
    border-radius: 0.3em;
    padding: .6rem;
  }
  h1 {
    text-align: left;
  }
  img {
    max-width: 100% !important;
  }
</style>
