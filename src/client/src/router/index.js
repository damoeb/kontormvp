import Vue from 'vue'
import Router from 'vue-router'
import Inbox from '@/components/Inbox'
import Reader from '@/components/Reader'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/inbox'
    },
    {
      path: '/inbox',
      component: Inbox
    },
    {
      path: '/reader/:articleId',
      component: Reader
    },
    {
      path: '/inbox/:folderId',
      component: Inbox
    }
  ]
})
