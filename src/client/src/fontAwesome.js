import Vue from 'vue'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faChevronRight, faStar, faTag, faPlus, faBars, faLayerGroup, faCog, faHome, faUser, faHeart} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(faChevronRight, faStar, faTag, faPlus, faBars, faLayerGroup, faCog, faHome, faUser, faHeart)

Vue.component('font-awesome-icon', FontAwesomeIcon)
