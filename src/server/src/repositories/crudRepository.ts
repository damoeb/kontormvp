import {ObjectID} from 'bson';
import mongo from '../mongo';
import {reduce} from 'lodash';
import {Article} from './articleRepository';
import logger from '../logger';
import {Cursor} from "mongodb";

export interface Id {
  _id: ObjectID,
  createdAt?: Date,
  updatedAt?: Date,
}

export interface Harvestable {
  harvestable: boolean
}

export interface Tagable {
  tags: string[]
}

export abstract class CRUDRepository<T extends Id> {

  protected constructor(private collection: string) {

  }

  protected createUniqueIndex(fields: string[]) {
    mongo.collection<Article>(this.collection)
      .then(collection => {
        const indexSpec = reduce(fields, (result: any, field) => {
          result[field] = 1;
          return result;
        }, {});
        collection.createIndex(indexSpec, {unique: true}, (err) => {
          if (err) {
            logger.error(`Failed to verify/create unique-indexes on ${this.collection}: ${err}`);
          }
        });
      });
  }

  protected createIndex(fields: string[]) {
    mongo.collection<Article>(this.collection)
      .then(collection => {
        const indexSpec = reduce(fields, (result: any, field) => {
          result[field] = 'text';
          return result;
        }, {});
        collection.createIndex(indexSpec, (err) => {
          if (err) {
            logger.error(`Failed to verify/create text-indexes on ${this.collection}: ${err}`);
          }
        });
      });
  }

  find(query: object, orderBy: object = {}, from: number = 0, count: number = 100, projection: object = undefined): Promise<Cursor<T>> {
    return new Promise<Cursor<T>>((resolve) => {
      return mongo.collection<T>(this.collection).then(collection => {
        resolve(collection.find(query, {projection}).sort(orderBy).skip(from).limit(count));
      });
    });
  }

  delete(query: object): Promise<Number> {

    return new Promise((resolve, reject) => {
      mongo.collection<T>(this.collection).then(collection => {
        collection.deleteMany(query, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result.deletedCount)
          }
        });
      });
    });
  }

  get(id: ObjectID): Promise<T> {
    return mongo.collection<T>(this.collection).then(collection => {
      return collection.findOne<T>({_id: id});
    });
  }

  // todo return a stirng instead of promise, keep that ObjectId stuff encapsulated
  create(entity: Partial<T>): Promise<Id> {
    return new Promise<Id>((resolve, reject) => {

      // todo validate the correct properties are set
      const entityFull: T = <T> entity;

      mongo.collection<T>(this.collection).then(collection => {
        collection.insertOne(entityFull, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve({_id: new ObjectID(result.insertedId)});
          }
        });
      })
    });
  }

  update(entity: Partial<T>): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      mongo.collection<T>(this.collection).then(collection => {
        collection.updateOne({_id: entity._id}, {$set: entity}, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve({modifiedCount: result.modifiedCount});
          }
        });
      })
    });
  }
}
