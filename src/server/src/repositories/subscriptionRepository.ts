import {CRUDRepository, Harvestable, Id, Tagable} from './crudRepository';

export interface Subscription extends Id, Tagable, Harvestable {
  userId: string
  htmlUrl: string
  // todo add a field to configure the amount all-you-can eat
  // todo add a field to warn that a feed is spamming
  reason?: string // why you want to subscribe
  feedUrl: string
  group: string // subscription group
  title: string
  createdAt: Date
  textFilter: string // implement filter
}

/*
GET ARTICLES PROCESS

Problems:
- User subscribes to a feed after it was harvested
- User subscribes to a feed that never has been harvested yet
- Custom tags might be added to a harvested article
- A filter might be arbitrarily complex and async
- A filter should be constructed from some sandboxed javascript function, for example the text filter is `indexOf(article.content, 'WELTwoche') > -1`
- Accessing a feed will list its articles below

Solution:
- cache every harvested raw article (url, data)

Process:
1. Subscribe feed-url `f` with text-filter 'WELTJournal'
2. Subscribe feed-url `f` with text-filter 'Schauplatz'
3. Cronjob looks for new feed-urls in subscription table
4. Cronjob then downloads the feed `f`
5. Cronjob iterates over all articles of `f` and pushes them to the unresolved_articles table
6. Cronjob resolves the readability for articles and pushes them to `f`-subscribers, filters the articles and pushes the articles to the articles table

 */


export class SubscriptionRepository extends CRUDRepository<Subscription> {

  constructor() {
    super('subscriptions');
    this.createUniqueIndex(['userId', 'feedUrl']);
  }
}

export default new SubscriptionRepository();
