import {CRUDRepository, Id} from './crudRepository';
import {Subscription} from "./subscriptionRepository";

export interface User extends Id {
  username?: string,
  createdAt?: Date,
  subscriptions?: Subscription[]
}

export class UserRepository extends CRUDRepository<User> {

  constructor() {
    super('users');
    this.createUniqueIndex(['username']);
  }
}

export default new UserRepository();
