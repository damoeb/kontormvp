import {CRUDRepository, Harvestable, Id, Tagable} from './crudRepository';
import {Article} from './articleRepository';

export interface Feed extends Id, Tagable, Harvestable {
  // todo add stats like rel articles/t
  url?: string,
  name?: string,
  createdAt?: Date,
  lastSuccess?: Date,
  lastFailure?: Date,
  subscriberCount?: number,
  items?: Article[]
}

export class FeedRepository extends CRUDRepository<Feed> {

  constructor() {
    super('feeds');
    this.createIndex(['name', 'url']);
    this.createUniqueIndex(['url']);
  }
}

export default new FeedRepository();
