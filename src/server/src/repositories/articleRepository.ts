import {CRUDRepository, Id, Tagable} from './crudRepository';
import {ObjectID} from 'bson';

export interface ReadabilityArticle {
  title: string
  content: string // HTML string of processed article content
  textContent: string // HTML string of processed article content
  length: number // length of an article, in characters
  excerpt: string // article description, or short excerpt from the content
  byline: string //author metadata
  dir: string //content direction
}

export class Article implements Id, Tagable {

  // user fields
  rating: number = 0; // 0-1 reading progress todo implement
  progress: number = 0; // 0-1 reading progress
  archived: boolean = false;
  readLater: boolean = false;
  deleted: boolean = false;

  // content-fields
  htmlUrl: string;
  title: string;
  content: string;
  textContent: string;
  tags: string[] = [];
  author: string;
  excerpt: string;

  // read-only
  _id: ObjectID;
  feedUrl: string;
  domain: string;
  providedBy: string[];
  userId: string;

  // system
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  score: number = undefined;
}

export class ArticleRepository extends CRUDRepository<Article> {

  constructor() {
    super('articles');
    this.createUniqueIndex(['htmlUrl', 'userId']);
    this.createIndex(['title', 'textContent']);
  }
}

export default new ArticleRepository();
