import {CRUDRepository, Tagable} from './crudRepository';
import {ReadabilityArticle} from './articleRepository';
import {ObjectID} from "bson";

export interface IHarvestTaskResult {
  article: ReadabilityArticle
}

export interface IHarvestTaskData extends Tagable{
  feedUrl: string,
  articleUrl: string
}

export class Task {
  _id: ObjectID;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  lastPop: Date;
  errorsCount: number = 0;
  result: IHarvestTaskResult;
  errors: string[];

  constructor(public data: IHarvestTaskData) {

  }
}

export class TaskRepository extends CRUDRepository<Task> {

  constructor() {
    super('tasks');
    this.createUniqueIndex(['data.articleUrl']);
  }
}

export default new TaskRepository();
