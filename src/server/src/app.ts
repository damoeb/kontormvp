import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import * as socketIo from 'socket.io';

import mongo from './mongo'
import logger from './logger'
import socketService from './services/socketService';
import cronService from './services/cronService';
import articleEndpoint from './endpoints/articleEndpoint';
import {config} from './config';
import feedEndpoint from './endpoints/feedEndpoint';
import inboxEndpoint from './endpoints/inboxEndpoint';
import bootstrapEndpoint from './endpoints/bootstrapEndpoint';
import subscriptionEndpoint from './endpoints/subscriptionEndpoint';
import taskEndpoint from "./endpoints/taskEndpoint";
import toolsEndpoint from "./endpoints/toolsEndpoint";
import userEndpoint from "./endpoints/userEndpoint";

// -- express
const app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const server = http.createServer(app);
const io = socketIo(server);

export interface ErrorMsg {
  message: string,
  code: number
}

export interface ErrorsResponse {
  errors: ErrorMsg[]
}


// -- endpoints

io.on('connection', socketService.onConnection);

articleEndpoint.register(app);
bootstrapEndpoint.register(app);
feedEndpoint.register(app);
inboxEndpoint.register(app);
taskEndpoint.register(app);
subscriptionEndpoint.register(app);
toolsEndpoint.register(app);
userEndpoint.register(app);

// client

app.use('/', express.static('public'))

// startup

// via http://www.patorjk.com/software/taag/#p=display&f=Doom&t=kontor
const logo = `
 __                 __              
|  |--.-----.-----.|  |_.-----.----.
|    <|  _  |     ||   _|  _  |   _|
|__|__|_____|__|__||____|_____|__|  
                                    
version ${config.version}

`;

if (config.appEnv === 'prod') {
  console.log(logo);
}

cronService.start();

mongo.client().then(() => {
  server.listen(config.port, () => logger.info(`Listening on port ${config.port}`));
}).catch(logger.error);
