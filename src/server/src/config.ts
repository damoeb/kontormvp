export const config = {
  version: '0.1-alpha',
  appEnv: 'dev',
  port: 3000,
  hostname: 'localhost',
  db: 'kontor',
  logLevel: 'info',
  maxErrorCount: 2,
  mongoUrl: 'mongodb://localhost:27017/kontor'
};
