import {Collection, Db, MongoClient} from 'mongodb'

import logger from './logger';
import {config} from './config';

export class MongoConnection {
  private readonly clientPromise: Promise<MongoClient>;

  constructor() {
    this.clientPromise = new Promise((resolve, reject) => {
      const options = {useNewUrlParser: true};
      MongoClient.connect(config.mongoUrl, options, (err, client) => {
        if (err) {
          logger.error(err);
          reject(err);
          throw err;
        }
        logger.info(`mongodb using database '${config.db}'`);
        resolve(client);
      });
    });
  }

  client(): Promise<MongoClient> {
    return this.clientPromise;
  }

  db(): Promise<Db> {
    return this.client().then(client => {
      return client.db(config.db);
    });
  }

  collection<T>(collectionName: string): Promise<Collection<T>> {
    return this.client().then(client => {
      return client.db(config.db).collection(collectionName);
    });
  }

}

export const Order = {Asc: 1, Desc: -1};

export default new MongoConnection();
