export interface AtomFeedItem {
  pubdate: Date;
  categories: string[];
  summary: string;
  description: string;
  author: string;
  published: Date
  title: string
  link: string
}
