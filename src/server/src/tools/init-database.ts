import {each} from 'lodash';
import * as request from "request";
import {Subscription} from "../repositories/subscriptionRepository";
import {User} from "../repositories/userRepository";
import {UserService} from "../services/userService";

interface Group {
  name: string,
  tags?: string[],
  subscriptions: Subscription1[]
}

const Relevance = {
  ALL: 'ALL',
  SOME: 'SOME',
  ONLY_BEST: 'ONLY_BEST'
};

interface Subscription1 {
  title: string
  htmlUrl: string
  xmlUrl: string
  textFilter?: string
  text?: string,
  tags?: string[],
  harvestable?: boolean,
  relevance?: string
}

const TextType = {
  narrative: 'narrative',
  scientific: 'scientific',
  fictive: 'fictive'
};

const groups: Group[] = [
  {
    name: 'TV Channels',
    subscriptions: [
      // tv
      {
        title: 'Panorama NDR',
        htmlUrl: 'https://daserste.ndr.de/panorama/index.html',
        xmlUrl: 'http://daserste.ndr.de/panorama/panoramaindex141-rss.xml',
      },
      {
        title: 'Panorama Podcast NDR',
        htmlUrl: 'https://daserste.ndr.de/panorama/index.html',
        xmlUrl: 'https://www.ndr.de/podcast/panorama286.xml',
      },
      {
        title: 'Monitor ARD',
        htmlUrl: 'https://www1.wdr.de/daserste/monitor/index.html',
        xmlUrl: 'https://www1.wdr.de/daserste/monitor/indexmonitor100.feed',
      },
      // todo requires youtube parser
      {
        title: 'vpro Documentary',
        htmlUrl: 'https://www.vprobroadcast.com/',
        xmlUrl: 'https://www.youtube.com/user/VPROinternational/videos',
      },
      // todo requires radiopublic.com parser
      {
        title: 'Frontline PBS',
        htmlUrl: 'https://www.pbs.org/wgbh/frontline/podcasts/dispatch/',
        xmlUrl: 'https://radiopublic.com/FRONTLINEDispatch',
      },
      {
        title: 'Weltjournal ORF',
        htmlUrl: 'https://tv.orf.at/weltjournal/',
        xmlUrl: 'https://rss.orf.at/tv.xml',
        textFilter: 'WELTjournal',
        harvestable: false
      },
      // new Outline('https://svt.se', 'https://www.svt.se/rss.xml'),
      // new Outline('https://yle.fi'),
      // new Outline('https://www.arte.tv/'),
    ]
  },
  // {
  //   name: 'News',
  //   subscriptions: [
  //     {
  //       title: 'Zeit Online',
  //       htmlUrl: 'https://www.zeit.de/index',
  //       xmlUrl: 'https://newsfeed.zeit.de/index',
  //     },
  //     {
  //       title: 'Le Monde',
  //       htmlUrl: 'https://www.lemonde.fr/',
  //       xmlUrl: 'https://www.lemonde.fr/rss/une.xml',
  //     },
  //     {
  //       title: 'heise.de',
  //       htmlUrl: 'https://heise.de',
  //       xmlUrl: 'https://www.heise.de/newsticker/heise-atom.xml',
  //     },
  //     {
  //       title: 'politiken.dk',
  //       htmlUrl: 'https://politiken.dk/',
  //       xmlUrl: 'https://politiken.dk/rss/senestenyt.rss',
  //     },
  //     {
  //       title: 'Slate',
  //       htmlUrl: 'https://slate.com/',
  //       xmlUrl: 'http://www.slate.com/articles.fulltext.all.10.rss',
  //     },
  //     {
  //       title: 'The Atlantic',
  //       htmlUrl: 'https://www.theatlantic.com/',
  //       xmlUrl: 'https://www.theatlantic.com/feed/all/',
  //     },
  //     {
  //       title: 'The Guardian',
  //       htmlUrl: 'https://www.theguardian.com/',
  //       xmlUrl: 'https://www.theguardian.com/international/rss',
  //     },
  //     {
  //       title: 'Telepolis',
  //       htmlUrl: 'https://telepolis.de',
  //       xmlUrl: 'http://www.heise.de/tp/rss/news-xl.xml',
  //     },
  //     {
  //       title: 'Deutsche Welle',
  //       htmlUrl: 'https://www.dw.com/',
  //       xmlUrl: 'http://rss.dw.com/rdf/rss-en-all',
  //     },
  //     {
  //       title: 'The Hill',
  //       htmlUrl: 'http://thehill.com',
  //       xmlUrl: 'http://thehill.com/rss/syndicator/19110',
  //     },
  //     {
  //       title: 'New Republic',
  //       htmlUrl: 'https://newrepublic.com',
  //       xmlUrl: 'https://newrepublic.com/rss.xml',
  //     },
  //     {
  //       title: 'The Region',
  //       htmlUrl: 'https://theregion.org/',
  //       xmlUrl: 'https://theregion.org/feed/all.xml',
  //     },
  //     {
  //       title: 'The Conversation',
  //       htmlUrl: 'https://theconversation.com/',
  //       xmlUrl: 'https://theconversation.com/articles.atom?language=en',
  //     },
  //     {
  //       title: 'Al Monitor',
  //       htmlUrl: 'https://www.al-monitor.com/',
  //       xmlUrl: 'https://www.al-monitor.com/rss',
  //       text: 'The pulse of the middle east',
  //     },
  //     //todo requires parser or mail subscription
  //     // new Outline('Mic', 'https://mic.com/'),
  //   ]
  // },
  {
    name: 'Blogs',
    subscriptions: [
      {
        title: 'Nachdenkseiten',
        htmlUrl: 'https://www.nachdenkseiten.de',
        xmlUrl: 'https://www.nachdenkseiten.de/?feed=rss2'
      },
      {
        title: 'Das Blättchen',
        htmlUrl: 'https://das-blaettchen.de',
        xmlUrl: 'https://das-blaettchen.de/feed'
      },
      {
        title: 'International Coorporation of Investigative Journalists',
        htmlUrl: 'https://www.icij.org/',
        xmlUrl: 'https://www.icij.org/feed/?post_type=post'
      },
      // pay wall
      // {
      //   title: 'Addendum',
      //   htmlUrl: 'https://www.addendum.org/',
      //   xmlUrl: 'https://www.addendum.org/feed/rss2-addendum'
      // },
      {
        title: 'Keimform.de',
        htmlUrl: 'http://keimform.de/',
        xmlUrl: 'http://keimform.de/feed/'
      },
      {
        title: 'PRAXIS',
        htmlUrl: 'https://praxis.fortelabs.co/',
        xmlUrl: 'https://praxis.fortelabs.co/feed/'
      },

      {
        title: 'Hackermoon',
        htmlUrl: 'https://hackernoon.com/',
        xmlUrl: 'https://hackernoon.com/feed'
      },
      // new Outline('https://news.ycombinator.com'),
      {
        title: 'Propagandaschau',
        htmlUrl: 'https://propagandaschau.wordpress.com/',
        xmlUrl: 'https://propagandaschau.wordpress.com/feed/'
      },
      // new Outline('Seniora.org', 'https://www.seniora.org/'),
      // new Outline('Council on Foreign Relations', 'https://www.cfr.org/'),
      {
        title: 'Slate Star Codex',
        htmlUrl: 'http://slatestarcodex.com/',
        xmlUrl: 'http://slatestarcodex.com/feed/'
      },
      {
        title: 'New Left Review',
        htmlUrl: 'https://newleftreview.org/',
        xmlUrl: 'https://newleftreview.org/feed'
      },
      {
        title: 'der Freitag',
        htmlUrl: 'https://www.freitag.de/',
        xmlUrl: 'https://www.freitag.de/RSS'
      },
      {
        title: 'Augen Geradeaus',
        htmlUrl: 'https://augengeradeaus.net/',
        xmlUrl: 'https://augengeradeaus.net/feed/'
      },
      {
        title: 'KenFM',
        htmlUrl: 'https://kenfm.de/',
        xmlUrl: 'https://kenfm.de/feed/'
      },
      {
        title: 'Infosperber.ch',
        htmlUrl: 'https://www.infosperber.ch/',
        xmlUrl: 'https://www.infosperber.ch/inc/rss.cfm'
      }, // should work
      {
        title: 'Blätter',
        htmlUrl: 'https://www.blaetter.de/',
        xmlUrl: 'https://www.blaetter.de/rss.xml'
      },
      {
        title: 'Kontext TV',
        htmlUrl: 'http://www.kontext-tv.de/',
        xmlUrl: 'http://www.kontext-tv.de/de/rss.xml'
      },
      {
        title: 'Blickpunkt WiSo',
        htmlUrl: 'https://www.blickpunkt-wiso.de/',
        xmlUrl: 'https://www.blickpunkt-wiso.de/feed'
      },
      {
        title: 'Junge Welt',
        htmlUrl: 'https://www.jungewelt.de/',
        xmlUrl: 'https://www.jungewelt.de/feeds/newsticker.rss'
      },
      {
        title: 'Lobby Control',
        htmlUrl: 'https://www.lobbycontrol.de/',
        xmlUrl: 'https://www.lobbycontrol.de/feed/'
      },
      {
        title: 'Zebralogs',
        htmlUrl: 'https://zebralogs.wordpress.com/',
        xmlUrl: 'https://zebralogs.wordpress.com/feed/'
      },
      {
        title: 'Netzpolitik.org',
        htmlUrl: 'https://netzpolitik.org/',
        xmlUrl: 'https://netzpolitik.org/feed/'
      },
      {
        title: 'Unsere Zeit',
        htmlUrl: 'https://www.unsere-zeit.de/',
        xmlUrl: 'http://www.unsere-zeit.de/de/0001/rss'
      },
      {
        title: 'Cives',
        htmlUrl: 'https://cives.de/',
        xmlUrl: 'https://cives.de/feed'
      },
      {
        title: 'Deutschlandfunk',
        htmlUrl: 'https://www.deutschlandfunk.de/',
        xmlUrl: 'https://www.deutschlandfunk.de/podcast-hintergrund.725.de.podcast.xml'
      },
      // new Outline('https://correctiv.org/'),
      {
        title: 'The Intercept',
        htmlUrl: 'https://theintercept.com/',
        xmlUrl: 'https://theintercept.com/feed/?lang=en'
      },
      {
        title: 'attac Theorieblog',
        htmlUrl: 'http://theorieblog.attac.de/',
        xmlUrl: 'http://theorieblog.attac.de/feed/'
      },
    ]
  },
  {
    name: 'Science',
    tags: ['science'],
    subscriptions: [
      {
        title: 'Nautilus',
        htmlUrl: 'http://nautil.us/blog',
        xmlUrl: 'http://nautil.us/rss/all'
      },
      {
        title: 'Spektrum.de',
        htmlUrl: 'https://www.spektrum.de/',
        xmlUrl: 'https://www.spektrum.de/alias/rss/spektrum-de-rss-feed/996406',
        tags: [TextType.scientific]
      },
      {
        title: 'mosaic',
        htmlUrl: 'https://mosaicscience.com',
        xmlUrl: 'https://mosaicscience.com/feed/rss.xml',
        tags: [TextType.scientific]
      },
      {
        title: 'SEED',
        htmlUrl: 'http://seedmagazine.com/',
        xmlUrl: 'http://seedmagazine.com/feeds/RSS/',
        tags: [TextType.scientific]
      },
      {
        title: 'Undark',
        htmlUrl: 'https://undark.org/',
        xmlUrl: 'https://undark.org/feed/',
        tags: [TextType.scientific]
      },
      {
        title: 'quanta magazine',
        htmlUrl: 'https://www.quantamagazine.org/',
        xmlUrl: 'https://api.quantamagazine.org/feed/',
        tags: [TextType.scientific]
      },
      {
        title: 'National Public Radio',
        htmlUrl: 'http://www.npr.org',
        xmlUrl: 'https://www.npr.org/rss/rss.php?id=1001',
        tags: [TextType.narrative]
      },
      {
        title: 'Mind Hacks',
        htmlUrl: 'https://mindhacks.com',
        xmlUrl: 'https://mindhacks.com/feed/'
      },
    ]
  },

  // Products
  // new Outline('hypothesis.is','https://web.hypothes.is/blog', 'https://web.hypothes.is/feed/', 'Annotation tool for the web'),

  {
    name: 'AI',
    tags: ['ai'],
    subscriptions: [
      {
        title: 'Institute of Phycics Blog',
        htmlUrl: 'http://www.iopblog.org',
        xmlUrl: 'http://www.iopblog.org/feed/'
      },
      {
        title: 'AI Trends',
        htmlUrl: 'https://aitrends.com/',
        xmlUrl: 'https://aitrends.com/feed/'
      },
      {
        title: 'Open AI Blog',
        htmlUrl: 'https://blog.openai.com/',
        xmlUrl: 'https://blog.openai.com/rss/'
      },
      {
        title: 'Towards Data Science',
        htmlUrl: 'https://towardsdatascience.com/',
        xmlUrl: 'https://towardsdatascience.com/feed',
        textFilter: 'Sharing concepts, text: ideas and codes'
      },
      {
        title: 'DataScienceCentral',
        htmlUrl: 'https://www.datasciencecentral.com/',
        xmlUrl: 'https://feeds.feedburner.com/FeaturedBlogPosts-DataScienceCentral'
      },
      {
        title: 'Algorithmia.com',
        htmlUrl: 'https://blog.algorithmia.com/',
        xmlUrl: 'https://blog.algorithmia.com/feed/'
      },
      {
        title: 'The Berkeley Artificial Intelligence Research Blog',
        htmlUrl: 'http://bair.berkeley.edu',
        xmlUrl: 'https://bair.berkeley.edu/blog/feed.xml'
      },
      {
        title: 'bocoup Blog',
        htmlUrl: 'https://bocoup.com/blog',
        xmlUrl: 'https://bocoup.com/feed'
      },
    ]
  },
  {
    name: 'Events',
    subscriptions: [
      {
        title: 'Chaos Computer Club',
        htmlUrl: 'https://events.ccc.de/',
        xmlUrl: 'https://events.ccc.de/feed/'
      },
    ]
  },
  {
    name: 'Podcasts',
    tags: ['podcast'],
    subscriptions: [
      {
        title: 'The Good Fight Podcast',
        htmlUrl: 'http://www.slate.com/articles/podcasts/the_good_fight_podcast.html',
        xmlUrl: '',
        text: 'by Yascha Mounk'
      },
      {
        title: 'Alternativlos',
        htmlUrl: 'http://alternativlos.org/',
        xmlUrl: 'http://alternativlos.org/opus.rss',
        text: 'mit Frank und Fefe'
      },
      {
        title: 'Code Podcast',
        htmlUrl: 'https://codepodcast.com/',
        xmlUrl: 'http://feeds.soundcloud.com/users/soundcloud:users:201515747/sounds.rss'
      },
      {
        title: 'Only Human',
        htmlUrl: 'https://www.npr.org/podcasts/449020426/only-human',
        xmlUrl: 'https://feeds.feedburner.com/only-human'
      },
      {
        title: 'Giant Robots',
        htmlUrl: 'http://giantrobots.fm/',
        xmlUrl: 'https://rss.simplecast.com/podcasts/271/rss'
      },
      {
        title: 'You are not so smart',
        htmlUrl: 'https://youarenotsosmart.com/',
        xmlUrl: 'https://soundcloud.com/youarenotsosmart'
      },
      {
        title: 'INNOQ Podcast',
        htmlUrl: 'https://www.innoq.com/de/podcast/',
        xmlUrl: 'https://www.innoq.com/de/feed.atom',
        tags: ['engineering']
      },
      {
        title: 'Amsterdam Mamas',
        htmlUrl: 'https://amsterdam-mamas.nl/',
        xmlUrl: 'https://amsterdam-mamas.nl/rss.xml'
      },
      {
        title: 'Cortex',
        // description: 'Grey and Myke detail their workflows, interests, and paper cuts around the software and hardware they use',
        htmlUrl: 'https://www.relay.fm/cortex',
        xmlUrl: 'https://www.relay.fm/cortex/feed',
        // via: 'https://www.jacobyyoung.com/podcasts'
      },

    ]
  },
  {
    name: 'Individuals',
    subscriptions: [
      // todo the following items are actually reverse searches or aggregated feeds from several sources including twitter, blogs ..

      {
        title: 'Yannes Esposito',
        htmlUrl: 'http://yannesposito.com/Scratch/en/blog/',
        xmlUrl: 'http://feeds.feedburner.com/yannespositocomen',
        tags: ['eng'],
        relevance: Relevance.ALL
      },
      {
        title: 'Josha Bach',
        htmlUrl: 'http://bach.ai/',
        xmlUrl: 'http://bach.ai/feed.xml',
        tags: ['ai', 'physics']
      },
      {
        title: 'Hadmut Danisch',
        htmlUrl: 'http://www.danisch.de/blog/',
        xmlUrl: 'http://www.danisch.de/blog/feed/',
        tags: ['eng']
      },
      {
        title: 'Patrick McKenzie',
        htmlUrl: 'https://www.kalzumeus.com/',
        xmlUrl: 'https://www.kalzumeus.com/feed/articles/',
        tags: ['eng']
      },
      {
        title: 'Eric Rose',
        htmlUrl: 'https://www.grinchcentral.com/',
        xmlUrl: 'https://www.grinchcentral.com/feeds/all.atom.xml',
        tags: ['eng']
      },
      {
        title: 'Scott Helme',
        htmlUrl: 'https://scotthelme.co.uk/',
        xmlUrl: 'https://scotthelme.co.uk/rss/',
        tags: ['eng']
      },
      // #security
      {
        title: 'Matthias Monroy',
        htmlUrl: 'http://digit.so36.net/',
        xmlUrl: 'http://digit.so36.net/feed.atom',
        tags: ['eng']
      },
      {
        title: 'Frank Rieger',
        htmlUrl: 'https://frank.geekheim.de/',
        xmlUrl: 'https://frank.geekheim.de/?feed=rss2'
      },

      // Dirk Pohlmann?
      // Schauspieler Simon Schwarz?

      // new Outline('Yascha Mounk', 'https://www.yaschamounk.com/articles'),
      {
        title: 'Volker Birk',
        htmlUrl: 'https://blog.fdik.org',
        xmlUrl: 'https://blog.fdik.org/rss.xml'
      },
      {
        title: 'Norbert Häring',
        htmlUrl: 'http://norberthaering.de/',
        xmlUrl: 'http://norberthaering.de/de/?format=feed&amp;type=rss'
      },
      {
        title: 'Martin Ehrenhauser',
        htmlUrl: 'http://www.ehrenhauser.at/',
        xmlUrl: 'http://www.ehrenhauser.at/feed/'
      },
      {
        title: 'Daniel Lüdecke',
        htmlUrl: 'https://strengejacke.wordpress.com/',
        xmlUrl: 'https://strengejacke.wordpress.com/feed/'
      },
      {
        title: 'Robert Misik',
        htmlUrl: 'https://misik.at/',
        xmlUrl: 'https://misik.at/feed'
      },
      {
        title: 'Wolfgang Streeck',
        htmlUrl: 'https://wolfgangstreeck.com/',
        xmlUrl: 'https://wolfgangstreeck.com/feed/'
      },
      {
        title: 'Nick Heer',
        htmlUrl: 'https://pxlnv.com/',
        xmlUrl: 'https://pxlnv.com/feed'
      },
      {
        title: 'Zach Mandeville',
        htmlUrl: 'https://coolguy.website',
        xmlUrl: 'https://coolguy.website/updates.xml',
        tags: ['p2p'],
        // reason: 'p2p guy'
      },
    ]
  }
];

/*
Queries for your db or the internet

Evgeny Morozov, technoscepticist, http://evgenymorozov.com/writings.html
Joseph Weizenbaum, technoscepticist
Ivan Illich, technoscepticist

Arthur C. Clarke, futurist

Vannevar Bush, author of the “as we may think”

Richard Feynman, physicist

Peter Vlemmix, creator of movie "Panoptikum"

Wolfgang Streeck, author of article "https://neuesglobaleselend.blogspot.com/2010/08/refeudalisierung.html"

Nick Heer, author of https://pxlnv.com/blog/bullshit-web/

Thomas Piketty, author of book "Capital in the Twenty-First Century"

*/


const host = 'http://localhost:3000';

const users:Partial<User>[] = [
  { username: UserService.getSystemUserId() },
  { username: 'expert' },
  { username: 'beginner' },
];

each(users, user => {
  request({url: `${host}/api/users`, method: 'POST', json: true, body: user}, (error, serverResponse) => {
    if (error) {
      console.error(error);
    } else {
      console.log(serverResponse.body);
    }
  });
});


each(groups, group => {
  each(group.subscriptions, subscription => {
    const json: Partial<Subscription> = {
      htmlUrl: subscription.htmlUrl,
      feedUrl: subscription.xmlUrl,
      group: group.name,
      userId: 'expert',
      title: subscription.title,
      textFilter: subscription.textFilter,
      tags: subscription.tags
    };
    request({url: `${host}/api/subscriptions`, method: 'PUT', json: true, body: json}, (error, serverResponse) => {
      if (error) {
        console.error(error);
      } else {
        console.log(serverResponse.body);
      }
    });
  });
});
