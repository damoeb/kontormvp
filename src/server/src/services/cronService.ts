import logger from '../logger';
import feedService from "./feedService";

export default new class CronService {
  start() {
    logger.info('Cron jobs started');
    // setInterval(this.update, 500000);
    this.update();

  }

  update() {
    feedService.fetchFeedUpdates();
  }
}
