import {isEmpty, nth, trim} from 'lodash';

import {Article, ReadabilityArticle} from '../repositories/articleRepository';
import articleService, {ArticleService} from './articleService';
import logger from '../logger';
import {Order} from '../mongo';
import {config} from '../config';
import httpUtil from '../httpUtil';
import taskRepository, {Task, TaskRepository} from "../repositories/taskRepository";
import {ObjectID} from "bson";
import subscriptionService, {SubscriptionService} from "./subscriptionService";
import {UserService} from "./userService";
import feedService, {FeedService} from "./feedService";

export class TaskService {
  private articleService: ArticleService;
  private feedService: FeedService;
  private taskRepository: TaskRepository;
  private subscriptionService: SubscriptionService;

  constructor() {
    this.feedService = feedService;
    this.articleService = articleService;
    this.taskRepository = taskRepository;
    this.subscriptionService = subscriptionService;
  }

  queue(task: Task): void {
    if (isEmpty(task.data.articleUrl)) {
      throw new Error(`article url is null`);
    }
    this.taskRepository.find({'data.articleUrl': task.data.articleUrl})
      .then(cursor => cursor.limit(1).toArray())
      .then(tasks => {
        if (tasks.length === 0) {
          logger.info(`Queuing ${task.data.articleUrl}`);
          this.taskRepository.create(task).catch(logger.error);
        }
      })
  }

  pop(): Promise<Task> {
    return new Promise((resolve, reject) => {
      const query = {errorsCount: {$lt: config.maxErrorCount}};
      const orderBy = {errorsCount: Order.Asc, lastPop: Order.Asc};
      this.taskRepository.find(query, orderBy)
        .then(cursor => cursor.limit(100).toArray())
        .then((tasks: Task[]) => {
          const task: Task = nth(tasks, parseInt(`${Math.random() * 100}`, 10));
          if (task) {
            this.taskRepository.update({
              _id: task._id,
              errorsCount: task.errorsCount + 1,
              updatedAt: new Date(),
              lastPop: new Date()
            }).catch(logger.error);
            resolve(task);
          } else {
            // no articles without readability found
            reject()
          }
        }).catch(reject);
    });
  }

  async onReadability(task: Task, readability: ReadabilityArticle): Promise<void> {
    logger.info(`Receiving readability for '${task.data.articleUrl}''`);
    // create articles for subscriber of feedUrl and for default user
    const article = new Article();
    // article.htmlUrl = task.data.articleUrl;
    // article.feedUrl = task.data.feedUrl;
    // article.domain = httpUtil.extractDomain(task.data.articleUrl);
    // article.title = readability.title;
    article.userId = UserService.getSystemUserId();
    article.content = readability.content;
    article.textContent = trim(readability.textContent);
    // article.providedBy = [article.domain];
    // article.tags = task.data.tags || [];

    this.taskRepository.delete({_id: ObjectID.createFromHexString(task._id.toString())}).catch(logger.error);

    // todo should only update the content
    return articleService.createArticle(article, true).then(() => Promise.resolve());
  }

  onFailure(task: Task, error: string): Promise<void> {
    logger.info(`Receiving readability-error for '${task.data.articleUrl}', cause '${error}'`);
    const errors = task.errors || [];
    errors.push(error);
    const taskUpdate: Partial<Task> = {
      _id: task._id,
      errorsCount: task.errorsCount + 1,
      errors
    };
    return this.taskRepository.update(taskUpdate).then(() => Promise.resolve());
  }

  async getStatus() {
    // this.taskRepository.find({$count: {}})
    return {
      open: 12,
      withErrors: 3
    }
  }
}

export default new TaskService();
