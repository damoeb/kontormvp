import articleRepository, {Article, ArticleRepository} from '../repositories/articleRepository';
import {Id} from '../repositories/crudRepository';
import {ObjectID} from 'bson';
import {clone, isUndefined} from 'lodash';
import logger from '../logger';
import {Cursor} from "mongodb";
import subscriptionService, {SubscriptionService} from "./subscriptionService";
import {Subscription} from "../repositories/subscriptionRepository";
import {Order} from "../mongo";

export class ArticleService {
  private articleRepository: ArticleRepository;
  private subscriptionService: SubscriptionService;

  constructor() {
    this.articleRepository = articleRepository;
    this.subscriptionService = subscriptionService;
  }

  getInboxArticles(userId: string): Promise<Cursor<Article>> {
    const query = {
      archived: false,
      deleted: false,
      userId: userId
    };
    const orderBy = {score: Order.Desc, createdAt: Order.Desc};
    return this.articleRepository.find(query, orderBy, 0, 100, {content:0})
  }

  findArticles(query: object, orderBy: object, from: number = 0, count: number = 100): Promise<Cursor<Article>> {
    return this.articleRepository.find(query, orderBy, from, count, {content: 0})
  }

  getArticle(id: string): Promise<Article> {
    return this.articleRepository.get(new ObjectID(id))
  }

  async createArticle(article: Article, forwardToSubscribers = false): Promise<Id> {
    logger.info(`Creating article ${article.htmlUrl} for ${article.userId} ${forwardToSubscribers ? ' with forwarding' : '' }`);
    article.createdAt = article.createdAt || new Date();
    const id = await this.articleRepository.create(article);

    if (forwardToSubscribers) {
      // todo use kafka instead
      await subscriptionService.getSubscriptions(article.feedUrl)
        .then(cursor => cursor.toArray()) // todo inefficient
        .then(subscriptions => {
        logger.info(`Forwarding article ${article.htmlUrl} to ${subscriptions.length} subscribers of ${article.feedUrl}`);
        return subscriptions.map((subscription: Subscription) => {
          // add article for user
          const newArticle = clone(article);
          newArticle._id = undefined;
          newArticle.userId = subscription.userId;

          if (subscription.textFilter && article.textContent.indexOf(subscription.textFilter) === -1) {
            return Promise.resolve();
          }

          return this.createArticle(newArticle).catch(logger.error);
        });
      });
    }
    return id;
  }

  updateArticle(articleId: ObjectID, article: Partial<Article>): Promise<object> {
    if (!isUndefined(article)) {
      logger.debug(`Updating article ${articleId} with ${article}`);
      article._id = articleId;
      return this.articleRepository.update(article)
    }
  }
}

export default new ArticleService();
