import {Socket} from 'socket.io';
import {first, without} from 'lodash';
import taskService, {TaskService} from './taskService';
import feedService, {FeedService} from './feedService';
import logger from '../logger';

export class SocketService {
  private taskService: TaskService;
  private feedService: FeedService;
  private sockets:Socket[] = [];

  constructor() {
    this.taskService = taskService;
    this.feedService = feedService;
    this.onConnection = this.onConnection.bind(this);

    setInterval(this.pushStatus, 5000);
  }

  async pushStatus(): Promise<void> {
    // const taskStatus = await this.taskService.getStatus();
    // const feedStatus = await this.feedService.getStatus();
    //
    // this.broadcast({
    //   tasksOpen: taskStatus.open,
    //   tasksFailed: taskStatus.withErrors,
    //   feedCount: feedStatus.count
    // });
  }

  broadcast(stats:object): void {

    // todo how to broadcast
    const anySocket:Socket = first(this.sockets);
    if (anySocket) {
      anySocket.server.emit('status', stats)
    }
  }

  onConnection(socket: Socket): void {

    this.sockets.push(socket);

    logger.debug(`${socket.id} connected`);

    socket.on('error', (err) => {
      logger.error(`Socket ${socket.id} returned an error ${err}`);
    });

    socket.once('disconnect', () => {
      logger.debug(`${socket.id} left`);
      socket.disconnect(true);
      this.sockets = without(this.sockets, socket);
    });
  }
}

export default new SocketService();
