import userRepository, {User, UserRepository} from '../repositories/userRepository';
import {Cursor} from "mongodb";
import {Id} from "../repositories/crudRepository";

export class UserService {
  private userRepository: UserRepository;

  constructor() {
    this.userRepository = userRepository;
  }

  // getUser(id: string): Promise<Feed> {
  //   return null
  // }

  findUsers(query: object, orderBy: object = {}, from: number = 0, count: number = 100): Promise<Cursor<User>> {
    return this.userRepository.find(query, orderBy, from, count, {rawFeed: 0})
  }
  //
  // getArticle(id: string): Promise<Article> {
  //   return this.articleRepository.get(new ObjectID(id))
  // }

  updateUser(user: User):Promise<object> {
    return this.userRepository.update(user);
  }

  static getSystemUserId():string {
    return 'system';
  }

  createUser(user: User): Promise<Id> {
    // todo validate object
    return this.userRepository.create(user);
  }
}

export default new UserService();
