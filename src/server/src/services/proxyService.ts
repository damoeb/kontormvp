import * as request from 'request';

export default new class ProxyService {
  proxy(url: string): Promise<string> {
    return new Promise((resolve, reject) => {
      request(url, (error, serverResponse, body) => {
        if (!error && serverResponse && serverResponse.statusCode === 200) {
          resolve(body);
        } else {
          console.error(`proxy error ${url} cause ${error}, ${serverResponse.statusCode}`);
          reject(error);
        }
      });
    });
  }
}
