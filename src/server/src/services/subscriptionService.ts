import {flow, isEmpty} from 'lodash/fp';
import userService, {UserService} from './userService';
import subscriptionRepository, {Subscription, SubscriptionRepository} from "../repositories/subscriptionRepository";
import feedService, {FeedService} from "./feedService";
import {Cursor} from "mongodb";
import {Feed} from "../repositories/feedRepository";
import logger from "../logger";

export class SubscriptionService {
  private userService: UserService;
  private feedService: FeedService;
  private subscriptionRepository: SubscriptionRepository;

  constructor() {
    this.userService = userService;
    this.feedService = feedService;
    this.subscriptionRepository = subscriptionRepository;
  }

  unsubscribe(subscriptionId: string): Promise<Boolean> {
    // todo implement
    throw new Error('not implemented');
    // return userService.currentUser().then(user => {
    //   user.subscriptions = <Subscription[]> without(user.subscriptions, {subscriptionId});
    //   userService.updateUser(user);
    //   return true;
    // })
  }

  ensure(validator: (s: any) => boolean, object: any, propertyName: string) {
    if (!validator(object[propertyName])) {
      throw new Error(`'${propertyName} is empty'`);
    }
  }

  subscribe(subscription: Subscription): Promise<string> {
    let nonEmpty = flow(isEmpty, (isEmpty) => !isEmpty);
    this.ensure(nonEmpty, subscription, 'feedUrl');
    this.ensure(nonEmpty, subscription, 'title');
    this.ensure(nonEmpty, subscription, 'htmlUrl');
    this.ensure(nonEmpty, subscription, 'userId');
    return this.userService.findUsers({username: subscription.userId})
      .then(cursor => cursor.limit(1).toArray())
      .then(users => {
        if (users.length === 0) {
          throw new Error('User does not exist');
        }

      subscription.createdAt = new Date();

      // add if necessary to feeds table
      const feed: Partial<Feed> = {
        url: subscription.feedUrl,
        name: subscription.title,
        tags: subscription.tags,
        subscriberCount: 1
      };
      // todo check if feed already exists
      this.feedService.createFeed(feed).catch(logger.warn);

      logger.info(`User '${subscription.userId}' subscribing to '${subscription.title}' -> ${subscription.feedUrl}`);

      return this.subscriptionRepository.create(subscription).then((id) => id._id.toHexString());
    })
  }

  getSubscriptionsForUserId(userId:string): Promise<Cursor<Subscription>> {
    // todo should user is instead of username
    return this.subscriptionRepository.find({userId:userId});
  }

  getSubscriptions(feedUrl: string): Promise<Cursor<Subscription>> {
    return this.subscriptionRepository.find({feedUrl: feedUrl});
  }
};

export default new SubscriptionService();
