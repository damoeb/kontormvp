import * as request from 'request';
import {each, isNull, omitBy, trim, unescape, union} from 'lodash';
import * as FeedParser from 'feedparser';
import httpUtil from '../httpUtil';
import feedRepository, {Feed, FeedRepository} from '../repositories/feedRepository'
import logger from '../logger';
import {ObjectID} from 'bson';
import {Id} from '../repositories/crudRepository';
import {AtomFeedItem} from '../models/atom';
import articleService from './articleService';
import {Cursor} from "mongodb";
import taskService from "./taskService";
import {Task} from "../repositories/taskRepository";
import {Article} from "../repositories/articleRepository";

function compact(obj: object) {
  return omitBy(obj, (val: any, key: any) => {
    return isNull(val) || key.indexOf(':') > -1 || key === 'meta'
  });
}

export interface SiteAnalysis {
  feeds: Partial<Feed>[]
  title: string
}


export class FeedService {
  private feedRepository: FeedRepository;

  constructor() {
    this.feedRepository = feedRepository;
  }

  fetchXmlFeed(xmlUrl: string): Promise<Feed> {
    return new Promise((resolve, reject) => {

      const req = request(xmlUrl);
      const feedOptions: FeedParser.Options = {
        resume_saxerror: true
      };
      const feedParser = new FeedParser(feedOptions);

      req.on('error', function (error: string) {
        reject(new Error(`Failed to load the feed-url ${xmlUrl} with ${error}`));
      });

      req.on('response', function (res: any) {
        const stream = this; // `this` is `req`, which is a stream

        if (res.statusCode !== 200) {
          reject(new Error(`Bad status code ${res.statusCode} for feed-url ${xmlUrl}`));
        } else {
          stream.pipe(feedParser);
        }
      });

      const feed: Feed = {
        _id: null,
        tags: null,
        harvestable: null,
        url: xmlUrl,
        items: []
      };

      feedParser.on('end', function () {
        resolve(feed)
      });

      feedParser.on('error', function (error: string) {
        reject(new Error(`Failed to parse feed data from ${xmlUrl} with ${error}`));
      });

      feedParser.on('readable', function () {
        // // This is where the action is!
        const stream = this; // `this` is `feedParser`, which is a stream
        // const meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
        let item;
        //
        while (item = stream.read()) {
          feed.items.push(<any>compact(item))
        }
      });
    })
  }

  fetchFeedUpdates(): void {
    feedRepository.find({}).then(cursor => {
      cursor.hasNext().then(hasNext => {
        if (!hasNext) {
          logger.warn('You have no feeds!');
        }
      }).catch(() => { /* swallow error */
      });

      cursor.forEach(feed => {
        try {
          this.fetchXmlFeed(feed.url).then(jsonFeed => {
            each(<any> jsonFeed.items, (atomFeedItem: AtomFeedItem) => {
              articleService.findArticles({htmlUrl: atomFeedItem.link}, {}, 0, 1).then(cursor => {
                cursor.hasNext().then(articleExists => {
                  if (!articleExists) {

                    console.log(atomFeedItem);
                    const article = new Article();
                    article.createdAt = atomFeedItem.pubdate;
                    article.author = atomFeedItem.author;
                    article.tags = union(atomFeedItem.categories, feed.tags);
                    article.title = atomFeedItem.title;
                    article.htmlUrl = atomFeedItem.link;
                    article.content = atomFeedItem.description;
                    article.textContent = this.removeTags(atomFeedItem.description);
                    article.excerpt = this.removeTags(atomFeedItem.summary);
                    article.feedUrl = feed.url;
                    article.domain = httpUtil.extractDomain(atomFeedItem.link);
                    article.providedBy = [article.domain];

                    articleService.createArticle(article, true).catch(logger.error);
                    // // insert task instead
                    // const harvestTask = new Task({feedUrl: feed.url, articleUrl: atomFeedItem.link, tags: feed.tags});
                    //
                    // taskService.queue(harvestTask);
                  }
                });
              });
            });

            this.feedRepository.update({_id: feed._id, lastSuccess: new Date()}).catch(err => {
              logger.error(`Failed to update lastSuccess of feed ${feed.url}`, err);
            });

          }).catch(logger.error);
        } catch (err) {
          logger.error(`Failed to update feed ${feed.url}`, err);
          this.feedRepository.update({_id: feed._id, lastFailure: new Date()}).catch(err => {
            logger.error(`Failed to update lastFailure of feed ${feed.url}`, err);
          });
        }
      });
    }).catch(err => {
      console.error(`Failed to load feeds ${err}`);
    });
  }

  createFeed(feed: Partial<Feed>): Promise<Id> {
    return this.feedRepository.find({url: feed.url}).then(feeds => {
      logger.debug(`Creating feed ${feed.url}`);
      return feeds.hasNext().then(hasNext => {
        if (hasNext) {
          return Promise.reject(`feed ${feed.url} already exists`);
        } else {
          feed.createdAt = new Date();
          return this.feedRepository.create(feed);
        }
      })
    });
  }

  updateFeed(feed: Feed): Promise<object> {
    return this.feedRepository.update(feed)
  }

  getFeeds(query: object, from: number = 0, count: number = 100): Promise<Cursor<Feed>> {
    return feedRepository.find(query, {}, from, count);
  }

  getFeed(id: string): Promise<Feed> {
    return feedRepository.get(new ObjectID(id));
  }

  findFeedsInSite(url: string): Promise<SiteAnalysis> {
    if (!/^(http[s]?:\/\/)?[a-z0-9A-Z-._/]{3,}$/.test(url)) {
      logger.debug(`findFeedsInSite: Not a valid url ${url}`);
      return Promise.reject(`${url} is no url`);
    }
    if (/^http[s]?:\/\//.test(url)) {
      return this.findFeedsInSiteInternal(url)
    } else {
      // safe
      logger.debug(`findFeedsInSite: Trying https for ${url}`);
      return this.findFeedsInSiteInternal(`https://${url}`).catch(() => {
        // unsafe
        logger.debug(`findFeedsInSite: Falling back to http for ${url}`);
        return this.findFeedsInSiteInternal(`http://${url}`);
      })
    }
  }

  findFeedsInSiteInternal(url: string): Promise<SiteAnalysis> {
    return new Promise((resolve, reject) => {
      // TODO site should be accessed from a cache
      request(url, (error, serverResponse, body) => {
        // todo url could be the feed
        if (error) {
          reject(error);
        } else if (!error && serverResponse.statusCode === 200) {
          const linkTagRegexp = /<link[^<]+application\/(rss\+xml|atom\+xml)[^<]+>/ig;
          const titleTagRegexp = /<title>([^<]+)<\/title>/ig;
          const metaTagRegexp = /<meta\sproperty\s=\s"([^"]+)"\scontent\s=\s"([^"]+)"\s\/>/ig;

          const feeds = [];
          let m;
          do {
            m = linkTagRegexp.exec(body);
            if (m) {
              const match = m[0];
              try {
                const hrefRegexp = /href[ ]*=[ ]*"([^"]+)"/ig;
                const titleRegexp = /title[ ]*=[ ]*"([^"]+)"/ig;

                const domain = httpUtil.extractDomain(url);
                const feedUrl = this.toAbsoluteUrl(url.startsWith('https:'), domain, hrefRegexp.exec(match)[1]);

                let name;
                try {
                  name = unescape(titleRegexp.exec(match)[1]);
                } catch (err) {
                  name = domain;
                }

                feeds.push({url: feedUrl, name});
              } catch (err) {
                logger.error(`Cannot parse link-tag ${match}`)
              }
            }
          } while (m);

          do {
            m = metaTagRegexp.exec(body);
            if (m) {
              const match = m[0];
              console.log(match, m[1], m[2]);
            }
          } while (m);

          if (feeds.length === 0) {
            reject({errors: [{message: 'No rss/atom feeds found', code: 0}]})
          } else {
            try {
              resolve({feeds, title: trim(titleTagRegexp.exec(body)[1])});
            } catch (err) {
              resolve({feeds, title: ''});
            }
          }

        } else {
          console.error(`Received bad status ${serverResponse.statusCode} or error ${error} resolving url ${url}`);
          httpUtil.handleHttpErrors(error, serverResponse ? serverResponse.statusCode : -1, reject)
        }
      })
    });
  }

  private toAbsoluteUrl(isHttps: boolean, domain: string, feedUrl: string): string {
    if (feedUrl.startsWith('http:') || feedUrl.startsWith('https:')) {
      return feedUrl;
    }
    return `http${isHttps ? 's' : ''}://${domain}${feedUrl}`;
  }

  private removeTags(description: string = '') {
    return description.replace(/<[^>]+>/g, '');
  }
}

export default new FeedService();
