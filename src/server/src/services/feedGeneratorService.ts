export default new class FeedGeneratorService {

  getNodeName(element: HTMLElement, name: string): string {
    const newName = `${element.tagName}>${name}`;
    if (element.parentElement) {
      return this.getNodeName(element.parentElement, newName)
    } else {
      return newName
    }
  }

  filterCollection(collection: HTMLCollection) {
    return this.filterNodes(Array.prototype.slice.call(collection));
  }

  filterNodes(collection: HTMLElement[]) {
    return collection
      .filter(n => n.tagName == 'script')
      .filter((n: any) => n.text && n.text.length > 5)
  }
}
