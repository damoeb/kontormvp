import {Express} from 'express';
import {isString, union} from 'lodash';
import feedService from '../services/feedService';
import httpUtil from "../httpUtil";

export default new class FeedEndpoint {
  register(app: Express) {
    // for more metadata we might consider https://metascraper.js.org/#/

    app.get('/api/feeds/list', (request, response) => {
      const from = parseInt(request.param('from'));
      const count = parseInt(request.param('count'));
      const search = request.param('query');

      const query = isString(search) ? {$text: {$search: search}} : {};
      const findFeedsPromise = feedService.getFeeds(query, from, count).then(cursor => cursor.toArray());
      const findFeedsInSitePromise = feedService.findFeedsInSite(request.param('query')).then(siteAnalysis => siteAnalysis.feeds).catch(() => []);
      httpUtil.handlePromise(Promise.all([findFeedsPromise, findFeedsInSitePromise]).then(results => union(results[0], results[1])), response);
    });

    app.get('/api/feeds/:feedId', (request, response) => {
      const id = request.param('feedId');

      httpUtil.handlePromise(feedService.getFeed(id), response);
    });

    app.post('/api/feeds', (request, response) => {
      const feeds = request.body;

      httpUtil.handlePromise(feedService.createFeed(feeds), response);
    });
    app.put('/api/feeds', (request, response) => {
      const feeds = request.body;

      httpUtil.handlePromise(feedService.updateFeed(feeds), response);
    });
  }
}
