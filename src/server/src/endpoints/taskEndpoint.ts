import {Express} from 'express';
import httpUtil from "../httpUtil";
import taskService from "../services/taskService";

export default new class TaskEndpoint {
  register(app: Express) {

    app.get('/api/tasks/next', (request, response) => {
      httpUtil.handlePromise(taskService.pop(), response);
    });

    app.put('/api/tasks/:taskId', (request, response) => {
      const {task, readability} = request.body;

      httpUtil.handlePromise(taskService.onReadability(task, readability), response);
    });
  }
}
