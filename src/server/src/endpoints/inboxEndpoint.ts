import {Express} from 'express';
import articleService from '../services/articleService';
import {Order} from '../mongo';
import httpUtil from '../httpUtil';
import subscriptionService from '../services/subscriptionService';

export default new class InboxEndpoint {
  register(app: Express) {
    app.get('/api/inbox/:userId/list', (request, response) => {

      const userId = request.param('userId');

      const findInboxArticles = articleService.getInboxArticles(userId).then(cursor => cursor.project({content: 0, textContent: 0}).toArray());

      httpUtil.handlePromise(findInboxArticles, response);
    });
  }
}
