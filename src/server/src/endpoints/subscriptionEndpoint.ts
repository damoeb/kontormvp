import {Express} from 'express';
import httpUtil from '../httpUtil';
import subscriptionService from '../services/subscriptionService';
import {Subscription} from "../repositories/subscriptionRepository";

export default new class SubscriptionEndpoint {
  register(app: Express) {
    // create
    app.put('/api/subscriptions/', (request, response) => {
      const subscription: Subscription = request.body;
      httpUtil.handlePromise(subscriptionService.subscribe(subscription), response);
    });
    app.get('/api/subscriptions/:userId/list', (request, response) => {
      const userId = request.param('userId');
      const from = parseInt(request.param('from'));
      const count = parseInt(request.param('count'));
      httpUtil.handlePromise(subscriptionService.getSubscriptionsForUserId(userId).then(cursor => cursor.skip(from).limit(count).toArray()), response);
    });
    app.delete('/api/subscriptions/:subscriptionId', (request, response) => {
      httpUtil.handlePromise(subscriptionService.unsubscribe(request.param('subscriptionId')), response);
    });
  }
}
