import {Express} from 'express';
import httpUtil from '../httpUtil';
import userService from '../services/userService';
import subscriptionService from "../services/subscriptionService";
import articleService from "../services/articleService";

export default new class BootstrapEndpoint {
  register(app: Express) {
    // returns all data to bootstrap the site
    app.get('/api/page/_', (request, response) => {

      const profile = {
        // theme, ...
      };

      const userId = 'expert';


      const subscriptions = subscriptionService.getSubscriptionsForUserId(userId).then(cursor => cursor.toArray());
      const articlesInInbox = articleService.getInboxArticles(userId).then(cursor => cursor.toArray());
      httpUtil.handlePromise(Promise.all([subscriptions, articlesInInbox]).then(response => {return {subscriptions: response[0], articles: response[1]}}), response);
    });

  }
}
