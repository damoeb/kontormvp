import {Express} from 'express';
import httpUtil from "../httpUtil";
import feedService from "../services/feedService";

export default new class ToolsEndpoint {
  register(app: Express) {
    app.get('/api/tools/find-feed-in-site', (request, response) => {
      httpUtil.handlePromise(feedService.findFeedsInSite(request.param('url')), response);
    });

  }
}
