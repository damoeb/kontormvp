import {Express} from 'express';
import {ObjectID} from 'bson';
import articleService from '../services/articleService';
import httpUtil from '../httpUtil';

export default new class ArticleEndpoint {

  register(app: Express) {
    app.get('/api/articles/:userId/list', (request, response) => {
      const from = parseInt(request.param('from'));
      const count = parseInt(request.param('count'));
      const userId = request.param('userId');

      httpUtil.handlePromise(articleService.findArticles({userId: userId}, {}, from, count).then(cursor => cursor.toArray()), response);
    });

    app.get('/api/articles/:articleId', (request, response) => {
      const id = request.param('articleId');
      httpUtil.handlePromise(articleService.getArticle(id), response);
    });

    app.post('/api/articles', (request, response) => {
      const article = request.body;

      httpUtil.handlePromise(articleService.createArticle(article), response);
    });
    app.put('/api/articles/:articleId', (request, response) => {
      const articleId = request.param('articleId');
      const partialArticle = request.body;

      httpUtil.handlePromise(articleService.updateArticle(new ObjectID(articleId), partialArticle), response);
    });
  }
}
