import {Express} from 'express';
import userService from '../services/userService';
import httpUtil from "../httpUtil";

export default new class UserEndpoint {
  register(app: Express) {
    // for more metadata we might consider https://metascraper.js.org/#/

    // app.get('/api/users/list', (request, response) => {
    //   const from = parseInt(request.param('from'));
    //   const count = parseInt(request.param('count'));
    //   const search = request.param('query');
    //
    //   const query = isString(search) ? {$text: {$search: search}} : {};
    //   const findUsersPromise = userService.getUsers(query, from, count).then(cursor => cursor.toArray());
    //   httpUtil.handlePromise(findUsersPromise, response);
    // });

    // app.get('/api/users/:userId', (request, response) => {
    //   const id = request.param('userId');
    //
    //   httpUtil.handlePromise(userService.getUser(id), response);
    // });

    app.post('/api/users', (request, response) => {
      const users = request.body;

      httpUtil.handlePromise(userService.createUser(users), response);
    });
    // app.put('/api/users', (request, response) => {
    //   const users = request.body;
    //
    //   httpUtil.handlePromise(userService.updateUser(users), response);
    // });
  }
}
