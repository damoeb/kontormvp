#!/usr/bin/env bash
echo "-- Lint Dockerfiles"
find . -iname '*Dockerfile' -exec sh -c 'echo "{}"; docker run --rm -i hadolint/hadolint < {}' \;
